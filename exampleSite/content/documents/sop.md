---
title: "Standard System Build"
draft: false
tags: [operations, sop]
toc: true
description: In which the setup and management of systems is described
---

1. First you get the server
2. Plug it in
3. Install services
4. Make sure they stay up
5. Let your friends and colleagues know it's available
