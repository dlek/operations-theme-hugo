# "Operations" theme for Hugo

Simple theme for Hugo geared towards systems documentation for a small data
centre, such as for home.  Given the lack of documentation, you may [read the
original blog post](https://dlek.gitlab.io/posts/2019-04-21-documentation-plan/)
describing the intentions.

## Status

Functional but limited.

Major gaps:

* limited print and no mobile CSS
* partial documentation
* no suggested CI for automatically creating stubs for systems pages, or
  documentation to why that's necessary
* proper theme packaging such as example site, etc.  (There is a partial
  example site.)

## Using this theme

This theme is not intended as a general purpose Hugo theme, so using it for
your blog or political action group may result in frustration and a lack of
donations.  The three main content areas are documents, notes, and systems.

### Documents

Documents are authoritative content which concisely and more or less fully
describe their subject matter.  They have creation and last modification dates
but these are historical metadata and not contextually relevant.  If new
surrounding context, updated or replaced software, or other operational
parameters come into play, the content is updated in place to reflect this.
Examples of this type of content would include software and system
installation procedures, account lifecycle and onboarding/offboarding
procedures, and other "living" content which is used and updated as relevant
to the current environment.

For this type of content create a file
`content/documents/your-document-title.md` with front matter as in the
following template:

```
---
title: Your document title
draft: false
tags: [tag1, tag2]
toc: true
description: In which the a procedure is described
---
```

Follow this with content.

### Notes

Notes are shorter snippets recording important details about a work in
progress or simply less formal descriptions of first implementations, and the
like.  Notes may vary in length and reusability and come with the following
in-built excuses:

* YMMV
* Any documentation is better than none

Notes can be used as a development log or a collection of code snippets or
whatever.  Anything that might be useful later on or to somebody else but
without any analysis of intended audience and so on.

The same template as for documents is used but content files should be dated,
i.e. `content/notes/2019-10-14-how-I-did-that-thing.md`.

Ideally (but not always) notes written in executing a task will provide
development context for the eventual "document"-type content, so the longer
procedure can reference these notes so as to provide reasoning for implied
decisions without resulting in an overly long, exhaustive and exhausting
document.

### Systems

This type of content describes systems such as baremetal servers or LXD
containers or whatever assets are at play in your environment.  This is
accomplished using a bit of a hack on the Hugo content mechanisms.

Systems are described using two separate files in two separate areas.  For a
given system "sample.example.org", the following should be created in the site
root:

* `data/systems/sample.yml`
* `content/systems/sample.md`

The first can be copied from the supplied template `_template.yml` and is a
YAML file where the structure guides consistent recording of important
details.

> _Note:_ The level of detail captures the author's current needs and this is
> guaranteed to be inadequate for nearly every other user who comes along.
> You will almost certainly need to add fields to handle the details which are
> pertinent to your needs, and to add this to the theme's layout in order for
> it to be handled by Hugo.  Please consider submitting updates you make if
> you think they'll be useful for others.

The index page for Systems is based on the data content (that is, the YAML
descriptions in `/data/systems/*.yml`).  However, for individual systems pages
to be displayed, the second file must be present.  This can be a stub but
unfortunately is necessary for the full details of the system to be generated.
